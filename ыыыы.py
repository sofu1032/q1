#starting values
x = []
func = 42
#the number of lines in the file
with open('q1num.txt', 'r') as f:
    ln_count = 0
    for line in f:
        ln_count += 1
#working with values in a file
with open('q1num.txt', 'r') as file:
    for i in range(ln_count):
        #buff - буферная переменная
        buff = int(file.readline())
        x.append(buff)
#counting function
for i in range(len(x)):
    #num - one of the x values
    num = x[i]
    func = func+1/num
#function output
print(func)
